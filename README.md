# oasis-xrd

Types and rendering of [XRD] and [web-host] metadata.

Part of the [Fediverse] suite of protocols.

[XRD]: http://docs.oasis-open.org/xri/xrd/v1.0/xrd-1.0.html
[web-host]: https://tools.ietf.org/html/rfc6415
[Fediverse]: https://en.wikipedia.org/wiki/Fediverse

## XML

XML output of `tests/example/Main.hs` on example from Section B:

```xml
<XRD xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://docs.oasis-open.org/ns/xri/xrd-1.0">
    <Expires>
        1970-01-01T00:00:00Z
    </Expires>
    <Subject>
        http://example.com/gpburdell
    </Subject>
    <Property type="http://spec.example.net/type/person" xsi:nil="True"/>
    <Link href="http://services.example.com/auth" rel="http://spec.example.net/auth/1.0"/>
    <Link
      href="http://photos.example.com/gpburdell.jpg"
      rel="http://spec.example.net/photo/1.0"
      type="image/jpeg">
        <Title xml:lang="en">
            User Photo
        </Title>
        <Title xml:lang="de">
            Benutzerfoto
        </Title>
        <Property type="http://spec.example.net/created/1.0">
            1970-01-01
        </Property>
    </Link>
</XRD>
```

## JSON (JRD)

JSON output of `tests/example-json/Main.hs` on example from RFC 6415:


```json
{
    "subject": "http://blog.example.com/article/id/314",
    "expires": "1970-01-01T00:00:00Z",
    "aliases": [
        "http://blog.example.com/cool_new_thing",
        "http://blog.example.com/steve/article/7"
    ],
    "links": [
        {
            "titles": {
                "default": "About the Author",
                "en-us": "Author Information"
            },
            "href": "http://blog.example.com/author/steve",
            "type": "text/html",
            "rel": "author",
            "properties": {
                "http://example.com/role": "editor"
            }
        },
        {
            "titles": {
                "default": "The other author"
            },
            "href": "http://example.com/author/john",
            "rel": "author"
        },
        {
            "template": "http://example.com/copyright?id={uri}",
            "rel": "author"
        }
    ],
    "properties": {
        "http://blgx.example.net/ns/ext": null,
        "http://blgx.example.net/ns/version": "1.3"
    }
}
```
