{-# LANGUAGE OverloadedStrings #-}

module Utils
  ( theBeginning
  , forceURI
  ) where

import Data.Time (UTCTime, defaultTimeLocale, parseTimeOrError, iso8601DateFormat)

import qualified Data.XRD.Types as XRD

theBeginning :: UTCTime
theBeginning = parseTimeOrError
  False
  defaultTimeLocale
  (iso8601DateFormat $ Just "%H:%M:%S")
  "1970-01-01T00:00:00"

forceURI :: Either XRD.URIParseError a -> a
forceURI = either (error . show) id
