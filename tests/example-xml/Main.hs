{-# LANGUAGE OverloadedStrings #-}

-- | Example XRD document from http://docs.oasis-open.org/xri/xrd/v1.0/xrd-1.0.html#examples

module Main where

import qualified Data.Text.Lazy.IO as Text
import qualified Text.XML as XML

import Data.XRD.Types (XRD(..), Link(..))

import qualified Data.XRD.Types as XRD
import qualified Data.XRD.XML as XML

import Utils (forceURI, theBeginning)

main :: IO ()
main = Text.putStrLn
  . XML.renderText prettyRS
  $ XML.toDocument exampleXRD
  where
    prettyRS = XML.def
      { XML.rsPretty = True
      , XML.rsNamespaces =
        [ ("xsi", "http://www.w3.org/2001/XMLSchema-instance")
        ]
      }

exampleXRD :: XRD.XRD
exampleXRD = XRD.emptyXRD
  { xrdExpires = Just theBeginning
  , xrdSubject = either (error "bad subject") Just $
      XRD.subject "http://example.com/gpburdell"
  , xrdProperties =
      [ forceURI $ XRD.property
          "http://spec.example.net/type/person"
          Nothing
      ]
  , xrdLinks =
    [ XRD.emptyLink
        { linkRel = Just . forceURI $
            XRD.linkRelURI "http://spec.example.net/auth/1.0"
        , linkHref = Just . forceURI $
            XRD.uri "http://services.example.com/auth"
        }
    , XRD.emptyLink
        { linkRel = Just . forceURI $
            XRD.linkRelURI "http://spec.example.net/photo/1.0"
        , linkType = Just $ XRD.LinkType "image/jpeg"
        , linkHref = Just . forceURI $
            XRD.uri "http://photos.example.com/gpburdell.jpg"
        , linkTitles =
            [ XRD.Title (Just "en") "User Photo"
            , XRD.Title (Just "de") "Benutzerfoto"
            ]
        , linkProperties =
            [ forceURI $ XRD.property
                "http://spec.example.net/created/1.0"
                (Just "1970-01-01")
            ]
        }
    ]
  }
