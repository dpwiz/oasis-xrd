{-# LANGUAGE OverloadedStrings #-}

-- | Example JRD document from https://tools.ietf.org/html/rfc6415

module Main where

import Data.Aeson.Encode.Pretty (encodePretty)
import Data.XRD.Types (XRD(..), Link(..))

import qualified Data.XRD.Types as XRD
import qualified Data.XRD.JSON as JRD

import qualified Data.ByteString.Lazy.Char8 as BSLC

import Utils (forceURI, theBeginning)

main :: IO ()
main = BSLC.putStrLn
  . encodePretty
  $ JRD.toValue exampleJRD

exampleJRD :: XRD.XRD
exampleJRD = XRD.emptyXRD
  { xrdExpires = Just theBeginning
  , xrdSubject = either (error "bad subject") Just $
      XRD.subject "http://blog.example.com/article/id/314"
  , xrdAliases =
      [ forceURI $ XRD.subject "http://blog.example.com/cool_new_thing"
      , forceURI $ XRD.subject "http://blog.example.com/steve/article/7"
      ]
  , xrdProperties =
      [ forceURI $ XRD.property -- XXX: overwritten
          "http://blgx.example.net/ns/version"
          (Just "1.2")
      , forceURI $ XRD.property
          "http://blgx.example.net/ns/version"
          (Just "1.3")
      , forceURI $ XRD.property
          "http://blgx.example.net/ns/ext"
          Nothing
      ]
  , xrdLinks =
    [ XRD.emptyLink
        { linkRel = Just $
            XRD.LinkRelRegistered "author"
        , linkType = Just $
            XRD.LinkType "text/html"
        , linkHref = Just . forceURI $
            XRD.uri "http://blog.example.com/author/steve"
        , linkTitles =
            [ XRD.Title Nothing "About the Author"
            , XRD.Title (Just "en-us") "Author Information"
            ]
        , linkProperties =
            [ forceURI $ XRD.property
                "http://example.com/role"
                (Just "editor")
            ]
        }
    , XRD.emptyLink
        { linkRel = Just $
            XRD.LinkRelRegistered "author"
        , linkHref = Just . forceURI $
            XRD.uri "http://example.com/author/john"
        , linkTitles =
            [ XRD.Title Nothing "The other guy" -- XXX: overwritten
            , XRD.Title Nothing "The other author"
            ]
        }
    , XRD.emptyLink
        { linkRel = Just $
            XRD.LinkRelRegistered "author"
        , linkTemplate = Just "http://example.com/copyright?id={uri}"
        }
    ]
  }
