# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Compatible Versioning](https://github.com/staltz/comver).

## [Unreleased]

## [1.0] - 2018-04-07

### Added

- Initial release with XRD and JRD encoders

[1.0]: https://gitlab.com/dpwiz/oasis-xrd/tree/1.0
